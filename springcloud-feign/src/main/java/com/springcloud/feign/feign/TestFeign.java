package com.springcloud.feign.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @InterfaceName TestFeign
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/21 17:21
 * @Version 1.0
 **/
@FeignClient(value = "consul-service")
public interface TestFeign {
    @RequestMapping(value = "/hello")
    String hello();

    @RequestMapping(value = "/getServices")
    Object getServices();

    @GetMapping(value = "/base/weather/getWeather")
    String getWeather(@RequestParam("cityName")String cityName);

}
