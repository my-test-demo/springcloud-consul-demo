package com.springcloud.consul.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName Yesterday
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:18
 **/
@Data
public class Yesterday implements Serializable {
    private static final long serialVersionUID = 1L;

    private String date;
    private String highTemperature;
    private String windDirection;
    private String lowTemperature;
    private String windPower;
    private String type;
}
