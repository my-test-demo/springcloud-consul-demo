package com.springcloud.consul.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName Forecast
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:16
 **/
@Data
public class Forecast implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日期
     */
    private String date;
    /**
     * 最高温度
     */
    private String highTemperature;
    /**
     * 风向
     */
    private String windDirection;
    /**
     * 最低温度
     */
    private String lowTemperature;
    /**
     * 风力
     */
    private String windPower;
    /**
     * 天气类型
     */
    private String type;
}
