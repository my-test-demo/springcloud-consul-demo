package com.springcloud.consul.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName Wether
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:05
 **/
@Data
public class Weather implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 城市
     */
    private String city;

    private String aqi;
    /**
     * 温度
     */
    private String temperature;
    /**
     * 气候
     */
    private String climate;

    /**
     * 昨日天气
     */
    private Yesterday yesterday;
    /**
     * 未来几天天气预测
     */
    private List<Forecast> forecast;





}
