package com.springcloud.consul.service;

/**
 * @ClassName WeatherService
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:34
 **/
public interface WeatherService {
    String getWeather(String cityName);
}
