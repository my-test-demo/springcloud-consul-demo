package com.springcloud.consul.controller;

import com.springcloud.consul.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName WeatherController
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:40
 **/
@RestController
@RequestMapping("/base/weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @GetMapping("/getWeather")
    private String getWeather(@RequestParam("cityName") String cityName){
        return weatherService.getWeather(cityName);
    }

}
